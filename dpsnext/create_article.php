<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
include_once 'config/credential.php';
include_once 'config/endpoint.php';
include_once 'config/parameter.php';
include_once 'class/curl.php';
include_once 'class/helper.php';
include_once 'class/validate.php';
include_once  dirname( dirname(__FILE__) ) . '/dpsbridge_helper.inc';

// Load Drupal
$drupal_path = $_SERVER['DOCUMENT_ROOT'];
define('DRUPAL_ROOT', $drupal_path);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * UPDATE PARAMETERS BELOW								*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
$client_session_id = generate_uuid();

// Get metadata of the article
$article_metadata = $_POST['articleMeta'];

/**
 * The title of the article.
 * - the value must be UNIQUE
 * - used by create_article.php
 * - used by update_article.php
 * @param {string} $article_title
 * @example $article_title = 'This is the article title';
 *
 * Please uncomment below and input your value before use.
 */
$article_title = isset($article_metadata['title']) ? $article_metadata['title'] : '';
$entity_name = 'dpsbridge_' . $article_metadata['node']['nid'] . '_' . dpsbridge_helper_format_title($article_title);

/**
 * The access state of the article.
 * - use one of the following terms: free, metered, protected
 * - used by create_article.php
 * - used by update_article.php
 * @param {string} $article_accessState
 * @example $article_accessState = 'free'; // free article
 * @example $article_accessState = 'metered'; // metered article
 * @example $article_accessState = 'protected'; // protected article
 *
 * Please uncomment below and input your value before use.
 */
$article_accessState = 'protected';

/**
 * The link to related articles or contents.
 * - each link must be in reverse URL format
 * @param {array} $article_relatedContent - An array of reverse URLs to relative articles
 * @example $article_relatedContent = ['/com.adobe.prerelease/article/other_article;version=1412005728284'];
 *
 * CURRENTLY NOT USED!
 */
//$article_relatedContent = ['/com.adobe.prerelease/article/other_article;version=1412005728284'];

/**
 * The article byline text.
 * - used by create_article.php
 * - used by update_article.php
 * @param {string} $article_byline
 * @example $article_byline = 'By Adobe DPS';
 *
 * CURRENTLY NOT USED!
 */
$article_byline = isset($article_metadata['title']) ? $article_metadata['title'] : '';

/**
 * The keywords associated with the article.
 * - separate multiple keywords by a comma
 * - used by create_article.php
 * - used by update_article.php
 * @param {array} $article_keywords
 * @example $article_keywords = ['development'];
 * @example $article_keywords = ['development', 'test', 'api'];
 *
 * (OPTIONAL) Please uncomment below and input your value before use.
 */

$article_keywords = empty($article_metadata['node']) ? array() : array("DPSBridge-" . $article_metadata['node']['nid']);
if ($article_metadata['tags'] != '') {
  $keywords = explode(',', $article_metadata['tags']); 
  if (is_array($keywords)) {
    foreach ($keywords as $k) {
      $article_keywords[] = trim($k);
    }
  }  
}

/*
 * The priority of the article.
 * - use one of the following terms: low, normal, high.
 * - used by create_article.php
 * - used by update_article.php
 * @param {string} $article_importance
 * @example $article_importance = 'low'; // low priority
 * @example $article_importance = 'normal'; // normal priority
 * @example $article_importance = 'high'; // high priority
 *
 * Please uncomment below and input your value before use.
 */
$article_importance = 'normal';

/**
 * Specifies whether the article is an ad or not.
 * - set true to set the article as an ad, false otherwise
 * - used by create_article.php
 * - used by update_article.php
 * @param {boolean} $article_isAd - Specifies if the article is an ad or not
 * @example $article_isAd = true; // is an ad
 * @example $article_isAd = false; // not an ad
 *
 * Please uncomment below and input your value before use.
 */
$article_isAd = false;

/**
 * The article description field.
 * - used by create_article.php
 * - used by update_article.php
 * @param {string} $article_description
 * @example $article_description = 'This is the description field of an article.';
 *
 * Please uncomment below and input your value before use.
 */
$article_description = isset($article_metadata['title']) ? $article_metadata['title'] : '';;

/**
 * Specifies whether to show or hide the article from the browse page.
 * - set true to hide the article from the browse page, false otherwise
 * - used by create_article.php
 * - used by update_article.php
 * @param {boolean} $article_hideFromBrowsePage
 * @example $article_hideFromBrowsePage = true; // hide from browse page
 * @example $article_hideFromBrowsePage = false; // do NOT hide from browse page
 *
 * Please uncomment below and input your value before use.
 */
$article_hideFromBrowsePage = false;

/**
 * The article byline URL.
 * @param {string} $bylineUrl
 * @example $bylineUrl = 'http://Adobe.com';
 *
 * CURRENTLY NOT USED!
 */
$article_bylineUrl = 'http://www.adobe.com';

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * END OF PARAMETERS									*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
// takes arguments from the terminal, if available
//if (isset($argv) && count($argv) > 1) { include_once 'class/argv.php'; }

// custom validator for parameters and files
$validate = new validate();
// checks if the necessary parameters are available
$validate->param('credential', 'client_id', $client_id);
$validate->param('credential', 'client_version', $client_version);
$validate->param('parameter', 'entity_name', $entity_name);
$validate->param('parameter', 'entity_type', $entity_type);
$validate->param('parameter', 'publication_id', $publication_id);
// exists program if there's missing parameter
if ($validate->notValid()) { exit(); }

// use IDs if provided, otherwise auto-generate them
$client_request_id = isset($client_request_id) ? $client_request_id : generate_uuid();
$client_session_id = isset($client_session_id) ? $client_session_id : generate_uuid();
$show_input = isset($show_input) ? $show_input : false;
$show_debug = isset($show_debug) ? $show_debug : false;

$headers = array(
	'accept_type' => $content_type_json,
	'content_type' => $content_type_json,
	'client_id' => $client_id,
	'client_version' => $client_version,
	'request_id' => $client_request_id,
	'session_id' => $client_session_id,
);

$data = array(
	'entityName' => $entity_name,
	'entityType' => $entity_type
);

if ($article_title) 			 { $data['title'] = $article_title; }
if ($article_accessState) 		 { $data['accessState'] = $article_accessState; }
// if ($relatedContent) 		 { $data['relatedContent'] = $article_relatedContent; }
// if ($byline) 				 { $data['byline'] = $article_byline; }
if ($article_keywords) 			 { $data['keywords'] = $article_keywords; }
if ($article_importance) 		 { $data['importance'] = $article_importance; }
if ($article_isAd) 				 { $data['isAd'] = $article_isAd; }
if ($article_description) 		 { $data['description'] = $article_description; }
if ($article_hideFromBrowsePage) { $data['hideFromBrowsePage'] = $article_hideFromBrowsePage; }
// if ($article_bylineUrl) 		 { $data['bylineUrl'] = urlencode($article_bylineUrl); }

$url = $jupiter_endpoint . '/publication/' . $publication_id . '/' . $entity_type . '/' . $entity_name;

$curl = new curl('PUT', $url, $headers, $data);
$curl->exec();

$content_version_id_returned = $curl->get_content_version();
if ($content_version_id_returned !== FALSE) {
  $_SESSION['dps_article_content_version'] = $content_version_id_returned;
}
$version_id_returned = $curl->get_version_id();
if ($version_id_returned !== FALSE) {
  $_SESSION['dps_article_version_id'] = $version_id_returned;
  dpsbridge_helper_next_set_version($article_metadata['node']['nid'], $version_id_returned);
  echo $version_id_returned;
} 
//$curl->output($show_input, $show_debug);