<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
include_once 'config/credential.php';
include_once 'config/endpoint.php';
include_once 'config/parameter.php';
include_once 'class/curl.php';
include_once 'class/helper.php';
include_once 'class/validate.php';
include_once  dirname( dirname(__FILE__) ) . '/dpsbridge_helper.inc';

$client_session_id = generate_uuid();
$client_upload_id = generate_uuid();

// Get title of the article
$article_metadata = $_POST['articleMeta'];
$article_title = isset($article_metadata['title']) ? $article_metadata['title'] : '';

$entity_name = 'dpsbridge_' . $article_metadata['node']['nid'] . '_' . dpsbridge_helper_format_title($article_title);

// takes arguments from the terminal, if available
//if (isset($argv) && count($argv) > 1) { include_once 'class/argv.php'; }

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * CREATE ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
include 'create_article.php';
die();
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
$article_version_id = $curl->get_version_id();
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * INGEST .ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Ingest .Article File</h3>';
include 'upload_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
$article_version_id = $curl->get_version_id();
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * DELETE ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
//echo '<h3>Delete Article Entity</h3>';
//include 'delete_article.php';
