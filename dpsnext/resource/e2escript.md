### Create the article entity

```javascript
curl -X PUT
	-H "Accept: application/json"
	-H "X-dps-client-Session-Id: 3aee3cb0-d719-40bf-afe5-ad4ea15b27c9"
	-H "X-dps-Client-Version: 0.9.1.32153"
	-H "X-dps-Client-Id: postman"
	-H "Content-Type: application/vnd.adobe.entity+json"
	-H "x-dps-client-request-id: 6827bcdd-86b3-463a-9956-10b330ba02ab"
	-H "Cache-Control: no-cache"
	-H "Postman-Token: 47d8f561-0971-3810-7979-576aacf7f42e"
	-d '{
	        "title":"Adirondack Park, Forever Wild",
			"category":"feature articles",
			"accessState":"free",
			"keywords":["parks","New York","forest","update"],
			"entityId":"urn:com.viewerdemo.phone:article:stage01test1",
			"entityName":"stage01test1",
			"entityType":"article"
		}'
	"https://pecs-pub.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1"
```

### Upload the thumbnail image

Get the contentVersion from the previous response and use it as part of the API call path:

```javascript
curl -X PUT
	-H "Accept: application/json"
	-H "X-Session-Id: 3aee3cb0-d719-40bf-afe5-ad4ea15b27c9"
	-H "X-Client-Version: 0.9.1.32153"
	-H "X-Client-Id: postman"
	-H "Content-Type: image/jpeg"
	-H "x-dps-upload-id: 658c2c5b-b062-4c3b-bb47-a6b5637df6c5"
	-H "Cache-Control: no-cache"
	-H "Postman-Token: 2ec8f178-9e6e-69b2-4212-6cd0b38dd285"
	-d 'undefined'
	"https://pecs-pub.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1/contents;contentVersion=1423284293909/thumbnails/thumb1.jpg"
```


### Seal the content

```javascript
curl -X PUT
	-H "Accept: application/json"
	-H "X-Session-Id: 3aee3cb0-d719-40bf-afe5-ad4ea15b27c9"
	-H "X-Client-Version: 0.9.1.32153"
	-H "X-Client-Id: postman"
	-H "x-dps-upload-id: 658c2c5b-b062-4c3b-bb47-a6b5637df6c5"
	-H "Content-Type: application/json"
	-H "Cache-Control: no-cache"
	-H "Postman-Token: 91274402-eccb-53ef-abd7-b7791b4481b2"
	-d ''
	"https://pecs-pub.stage01.digitalpublishing.adobe.com/publication/com.viewerdemo.phone/article/stage01test1;version=1423284293909/contents"
```

### Update the article's metadata with the thumbnail info

```javascript
curl -X PUT
	-H "Accept: application/json"
	-H "X-dps-client-Session-Id: 3aee3cb0-d719-40bf-afe5-ad4ea15b27c9"
	-H "X-dps-Client-Version: 0.9.1.32153"
	-H "X-dps-Client-Id: postman"
	-H "Content-Type: application/vnd.adobe.entity+json"
	-H "x-dps-client-request-id: 6827bcdd-86b3-463a-9956-10b330ba02ab"
	-H "Cache-Control: no-cache"
	-H "Postman-Token: 47d8f561-0971-3810-7979-576aacf7f42e"
	-d '{
			"created": "2015-02-07T04:51:29Z",
			"accessState": "free",
			"category": "feature articles",
			"title": "Adirondack Park, Forever Wild",
			"keywords": [
				"parks",
				"New York",
				"forest",
				"update"
			],
			"entityId": "urn:com.fastco:article:stage01test1",
			"entityName": "stage01test1",
			"entityType": "article",
			"modified": "2015-02-07T04:54:27Z",
			"publicationID": "com.fastco",
			"_links": {
				"contentUrl": {
					"href": "/publication/com.fastco/article/stage01test1/contents;contentVersion=1423284867319/"
				},
				"thumbnail": {
					"width":513,
					"height":533,
					"href":"/publication/com.fastco/article/stage01test1/contents;contentVersion=1423284689577/thumbnails/thumb1.jpg"
				}
			}
		}
' https://pecs-pub.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1;version=1423284304404/contents
```

### Upload .article zip file

```javascript
curl -v -X PUT
	-H "Content-Type: application/vnd.adobe.folio+zip"
	-H "X-DPS-Client-Session-Id: 4be9b6e6-b894-4d40-b32c-669ef9d3ff88"
	-H "X-DPS-Client-Id: ingestion-service"
	-H "X-DPS-Client-Request-Id: 4be9b6e6-b894-4d40-b32c-669ef9d3ff99"
	-H "X-DPS-Client-Version: 0.1.0"
	-H "X-DPS-Upload-Id: e8fff1bc-44b4-4456-b7b5-0cf499b2baee"
	--data-binary @./00_Folio_Cover_v.article.zip
	"https://ings-pub.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1;version=1423285147940/contents/folio"
```

### Publish the article

```javascript
curl -i -X POST
	-H "Accept: application/json"
	-H "X-Session-Id: 3aee3cb0-d719-40bf-afe5-ad4ea15b27c9"
	-H "X-Client-Version: 9.30.0"
	-H "X-Client-Id: curl"
	-H "x-dps-client-request-id: 756b04ea-90fe-4c57-8dd2-f82b7e0fb7c6"
	-H "Content-Type: application/json"
	-d '{
			"workflowType":"publish",
			"entities":[
				"/publication/com.fastco/article/stage01test1;version=1423285620470"
			]
		}'
	"https://pecs-pub.stage01.digitalpublishing.adobe.com/job"
```

### Verify the published contents

```javascript
curl -v "https://eds.stage01.digitalpublishing.adobe.com/publication/com.fastco"
```

### Verify the article

```javascript
curl -v "https://eds.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1"
```

### Get the manifest for article from CDS

```javascript
curl -v -L "https://eds.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1/contents;contentVersion=1423285620435"
```

### Get the Article.xml

```javascript
curl -v -L "https://eds.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1/contents;contentVersion=1423285620435/Article.xml"
```

### Update thumbnail name

```javascript
curl -v -L "https://eds.stage01.digitalpublishing.adobe.com/publication/com.fastco/article/stage01test1/contents;contentVersion=1423285620435/thumbnails/thumb1.jpg" > thumb1.jpg
```