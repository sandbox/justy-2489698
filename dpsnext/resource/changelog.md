### 2/11/15

* ~~Add **upload\_assets** and **seal\_article\_content** API snippets~~
* Change the naming convention of **get\_article\_metadata** to **get\_article\_metadata**
* ~~In **article\_workflow**, append a **get\_article\_metadata** after each change to the article entity~~