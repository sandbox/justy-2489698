<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
include_once 'config/credential.php';
include_once 'config/endpoint.php';
include_once 'config/parameter.php';
include_once 'class/curl.php';
include_once 'class/helper.php';
include_once 'class/validate.php';

$client_session_id = generate_uuid();
$client_upload_id = generate_uuid();

$entity_name = 'article_workflow_dpci_102';

// takes arguments from the terminal, if available
if (isset($argv) && count($argv) > 1) { include_once 'class/argv.php'; }

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * CREATE ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Create Article Entity</h3>';
include 'create_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
$article_version_id = $curl->get_version_id();
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * INGEST .ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Ingest .Article File</h3>';
include 'upload_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
$article_version_id = $curl->get_version_id();
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * PUBLISH ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Publish Article Entity</h3>';
include 'publish_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * UPDATE ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Update Article Entity</h3>';
include 'update_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
$article_version_id = $curl->get_version_id();
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * PUBLISH ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
echo '<h3>Publish Article Entity</h3>';
include 'publish_article.php';
echo '<h3>Get Article Metadata</h3>';
include 'get_article_metadata.php';
/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
 * DELETE ARTICLE										*
 * -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
//echo '<h3>Delete Article Entity</h3>';
//include 'delete_article.php';