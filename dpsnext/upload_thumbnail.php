<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
include_once 'config/credential.php';
include_once 'config/endpoint.php';
include_once 'config/parameter.php';
include_once 'class/curl.php';
include_once 'class/helper.php';
include_once 'class/validate.php';

// takes arguments from the terminal, if available
if (isset($argv) && count($argv) > 1) { include_once 'class/argv.php'; }

// custom validator for parameters and files
$validate = new validate();
// checks if the necessary parameters are available
$validate->param('credential', 'client_id', $client_id);
$validate->param('credential', 'client_version', $client_version);
$validate->param('parameter', 'article_file', $article_file, true);
$validate->param('parameter', 'article_version_id', $article_version_id);
$validate->param('parameter', 'entity_name', $entity_name);
$validate->param('parameter', 'entity_type', $entity_type);
$validate->param('parameter', 'publication_id', $publication_id);
// exists program if there's missing parameter
if ($validate->notValid()) { exit(); }
// use IDs if provided, otherwise auto-generate them
$client_request_id = isset($client_request_id) ? $client_request_id : generate_uuid();
$client_session_id = isset($client_session_id) ? $client_session_id : generate_uuid();
$client_upload_id = isset($client_upload_id) ? $client_upload_id : generate_uuid();
$show_input = isset($show_input) ? $show_input : false;
$show_debug = isset($show_debug) ? $show_debug : false;

$headers = array(
	'accept_type' => $content_type_json,
	'content_type' => $content_type_image,
	'client_id' => $client_id,
	'client_version' => $client_version,
	'request_id' => $client_request_id,
	'session_id' => $client_session_id,
	'upload_id' => $client_upload_id,
);

$file_path = realpath($article_thumbnail);
$file_split = explode('/', $article_thumbnail);
$file_name = end($file_split);

$url = $jupiter_endpoint . '/publication/' . $publication_id . '/' . $entity_type . '/' . $entity_name . '/contents;contentVersion=' . $article_contentVersion . '/thumbnails/' . $file_name;

$curl = new curl('PUT', $url, $headers, $file_path, true);
$curl->exec();
$curl->output($show_input, $show_debug);

// Request is successful if nothing returned
if ($curl->response() === null || $curl->response() === '') {
  $_SESSION['dps_client_upload_id'] = $client_upload_id;
  
	echo '<pre>';
	print_r(array('message'=>'success'));
	echo '</pre>';
}