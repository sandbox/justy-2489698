<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
include_once 'config/credential.php';
include_once 'config/endpoint.php';
include_once 'config/parameter.php';
include_once 'class/curl.php';
include_once 'class/helper.php';
include_once 'class/validate.php';
include_once  dirname( dirname(__FILE__) ) . '/dpsbridge_helper.inc';

// Load Drupal
$drupal_path = $_SERVER['DOCUMENT_ROOT'];
define('DRUPAL_ROOT', $drupal_path);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

$article_metadata = $_POST['articleMeta'];
$article_title = isset($article_metadata['title']) ? $article_metadata['title'] : '';
$entity_name = 'dpsbridge_' . $article_metadata['node']['nid'] . '_' . dpsbridge_helper_format_title($article_title);

// artilce version id
$article_version_id = isset($_POST['versionID']) ? $_POST['versionID'] : '';


// takes arguments from the terminal, if available
//if (isset($argv) && count($argv) > 1) { include_once 'class/argv.php'; }

// custom validator for parameters and files
$validate = new validate();
// checks if the necessary parameters are available
$validate->param('credential', 'client_id', $client_id);
$validate->param('credential', 'client_version', $client_version);
//$validate->param('parameter', 'article_file', $article_file, true);
$validate->param('parameter', 'article_version_id', $article_version_id);
$validate->param('parameter', 'entity_name', $entity_name);
$validate->param('parameter', 'entity_type', $entity_type);
$validate->param('parameter', 'publication_id', $publication_id);
// exists program if there's missing parameter
if ($validate->notValid()) { exit(); }
// use IDs if provided, otherwise auto-generate them
$client_request_id = isset($client_request_id) ? $client_request_id : generate_uuid();
$client_session_id = isset($client_session_id) ? $client_session_id : generate_uuid();
$client_upload_id = isset($client_upload_id) ? $client_upload_id : generate_uuid();
$show_input = isset($show_input) ? $show_input : false;
$show_debug = isset($show_debug) ? $show_debug : false;

$headers = array(
	'accept_type' => $content_type_json,
	'content_type' => $content_type_folio,
	'client_id' => $client_id,
	'client_version' => $client_version,
	'request_id' => $client_request_id,
	'session_id' => $client_session_id,
	'upload_id' => $client_upload_id,
);


$dir = strstr(realpath(__FILE__), '/sites', TRUE);
$file_path = 'folio/' . dpsbridge_helper_format_title($article_title) . '.article';
$file_path = empty($file_path) ? NULL : $dir . '/sites/default/files' . '/dpsbridge/' . $file_path;
//$file_path = $article_metadata['dps_filepath'];
//$file_path = realpath($article_file);

$url = $jupiter_ingestion . '/publication/' . $publication_id . '/' . $entity_type . '/' . $entity_name . ';version=' . $article_version_id . '/contents/folio' ;
$curl = new curl('PUT', $url, $headers, $file_path, true);
$curl->exec();
//$curl->output($show_input, $show_debug);

// It will create new version ID, and content version ID if successful
$content_version_id_returned = $curl->get_content_version();
if ($content_version_id_returned !== FALSE) {
  $_SESSION['dps_article_content_version'] = $content_version_id_returned;
}
$version_id_returned = $curl->get_version_id();
if ($version_id_returned !== FALSE) {
  $_SESSION['dps_article_version_id'] = $version_id_returned;
  
  dpsbridge_helper_next_set_version($article_metadata['node']['nid'], $version_id_returned);
  echo $version_id_returned;
}
