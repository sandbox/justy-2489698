<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/

/**
 * An opaque string defining the version of the entity. Entity version ID is managed by service. For creation of a new entity, you must not specify version ID. Entity version ID is generated by service backend and immutable after creation. On updating an existing entity, entity version ID must be specified to match the head version of the entity. Entity version changes by service backend when the entity is successfully updated. If specified Version ID is incorrect, a version conflict error will be returned in API response.
 * - used by create_article.php
 * - used by delete_article.php
 * - used by publish_article.php
 * - used by update_article.php
 * - used by upload_article.php
 * @param {string} $article_version_id
 * @example $article_version_id = '1423505077141'
 *
 * Please uncomment below and input your value before use.
 */
$article_version_id = isset($_SESSION['dps_article_version_id']) ? $_SESSION['dps_article_version_id'] : '';

/**
 * You can get this value from the returned JSON data of the following API:
 * - create article API
 * @param {string} $article_contentUrl
 * @example $article_contentUrl = '/publication/com.viewerdemo.demo/article/com_mikey_2/contents;contentVersion=1423505077141/';
 *
 * Please uncomment below and input your value before use.
 */
$article_contentVersion = isset($_SESSION['dps_article_content_version']) ? $_SESSION['dps_article_content_version'] : '';

/**
 * The client defined name of an entity.
 * - used in "navto://" links
 * - can only contain letters, numbers, and the following special characters: _%.-
 * - the value must be UNIQUE
 * - used by create_article.php
 * - used by delete_article.php
 * - used by publish_article.php
 * - used by update_article.php
 * - used by upload_article.php
 * @param {string} $entity_name
 * @example $entity_name = 'TestArticleAPI_343';
 *
 * Please uncomment below and input your value before use.
 */
//$entity_name = 'sample_article_name_dpci_exp8';

/**
 * The relative path to the .article file location.
 * - used by upload_article.php
 * @param {string} $article_file
 * @example $article_file = 'article/00_Folio_Cover_v.article';
 *
 * Please uncomment below and input your value before use.
 */
//$article_file = 'article/00_Folio_Cover_v.article';

/**
 * The relative path to the JPEG image file location.
 * - used by upload_thumbnail.php
 * @param {string} $article_thumbnail
 * @example $article_thumbnail = 'image/thumb_P1.jpg';
 *
 * Please uncomment below and input your value before use.
 */
//$article_thumbnail = 'image/thumb_P1.jpg';

/**
 * The toggler to turn on or off the debugging statements.
 * - for debugging purposes ONLY
 * @param {boolean} $show_debug
 * @example $show_debug = true; // print debug statements
 * @example $show_debug = false; // don't print debug statements
 */
$show_debug = true;

/**
 * The toggler to turn on or off the debugging statements.
 * - for debugging purposes ONLY
 * @param {boolean} $show_input
 * @example $show_input = true; // print user inputs
 * @example $show_input = false; // don't print user inputs
 */
$show_input = false;

/**
 * DO NOT CHANGE THE BELOW VALUES:
 * - $entity_type = 'article';
 * - $content_type_all = 'application/json, text/plain';
 * - $content_type_article = 'application/vnd.adobe.folio+zip';
 * - $content_type_entity = 'application/vnd.adobe.entity+json';
 * - $content_type_image = 'image/jpg';
 * - $content_type_json = 'application/json';
 * - $content_type_zip = 'application/zip';
 */
$entity_type = 'article';
$content_type_all = 'application/json, text/plain, */*';
$content_type_article = 'application/vnd.article+zip';
$content_type_entity = 'application/vnd.adobe.entity+json';
$content_type_folio = 'application/vnd.adobe.folio+zip';
$content_type_image = 'image/jpg';
$content_type_json = 'application/json';
$content_type_zip = 'application/zip';