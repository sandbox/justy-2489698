<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/

/**
 * INTERNAL
 */
// $jupiter_endpoint = 'https://jupiter01-digitalpublishing.adobe.io/pecs/';
// $jupiter_ingestion = 'https://ings-jupiter01-lab-digitalpublishing.adobe.io';

// INTERNAL
// $jupiter_endpoint = 'https://pecs-pub-jupiter01.lab.digitalpublishing.adobe.com';
// $jupiter_ingestion = 'http://dps-ings.lab.digitalpublishing.adobe.com';

// STAGE 01
$jupiter_endpoint = 'https://pecs-pub.stage01.digitalpublishing.adobe.com';
$jupiter_ingestion = 'https://ings-pub.stage01.digitalpublishing.adobe.com';