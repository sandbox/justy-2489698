<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
/**
 * Client assigned string to be used to identify the client application.
 * Best practice is to use a reverse DNS style name.
 * @param {string} $client_id
 * @example $client_id = "SwaggerUI"
 *
 * Please uncomment below and input your value before use.
 */
//$client_id = 'dps-techmkt-exmpl';
$client_id = 'dps-drupal-dpci';

/**
 * The GUID unique to a request.
 * This ID is intended to identify all the API calls made to service a single, logical "user" request.
 * @param {string} $client_request_id
 * @example $client_request_id = '12345678-9abc-def0-1234-56789abcdef0'
 *
 * Please uncomment below and input your value before use.
 */
// $client_request_id = '756b04ea-90fe-4c57-8dd2-f82b7e0fb7c6';

/**
 * The GUID unique to a client-defined 'session'.
 * The scope of a session ID is defined by the client.
 * Recommendation is to use the same value for all requests from the same user login session.
 * @param {string} $client_session_id
 * @example $client_session_id = '87654321-0cba-9efg-4321-987650fedcba'
 *
 * Please uncomment below and input your value before use.
 */
// $client_session_id = '3aee3cb0-d719-40bf-afe5-ad4ea15b27c9';

/**
 * Specify the software version of the client application.
 * Best practice is to use double-dot style notation with a major, minor, and dot release number
 * @param {string} $client_version
 * @example $client_version = '1.3.5'
 *
 * Please uncomment below and input your value before use.
 */
$client_version = '0.0.1';

/**
 * The reverse DNS style name describing a publisher and publication.
 * PublicationID must match the regular expression pattern:
 * "[0-9a-zA-Z]{1,20}(\\.[0-9a-zA-Z]{1,20}){0,5}"
 * @param {string} $publication_id
 * @example $publication_id = 'com.adobe.inspiremagazine'
 *
 * Please uncomment below and input your value before use.
 */
$publication_id = 'com.prerelease.adobe';
$publication_id = 'com.prerelease.dpci';


// VALIDATION CODE

if (!isset($publication_id)) {
    echo "ERROR: Set the \$publication_id in credential.php<br>";
    exit();
}

if (!isset($client_id)) {
    echo "ERROR: Set the \$client_id in credential.php<br>";
    exit();
}

if (!isset($client_version)) {
    echo "ERROR: Set the \$client_version in credential.php<br>";
    exit();
}
?>