<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/

function format_date_custom($date) {
	$has_time = explode(' ', $date);
	$hour = 0;
	$minute = 0;
	$second = 0;

	if (count($has_time) === 2) {
		$date_array = explode('/', $has_time[0]);
		$time_array = explode(':', $has_time[1]);
		if (count($time_array) === 3) {
			$hour = $time_array[0];
			$minute = $time_array[1];
			$second = $time_array[2];
		} else {
			echo 'Wrong date + time format: mm/dd/yyyy hh:mm:ss';
			exit();
		}
	} else {
		$date_array = explode('/', $date);
	}

	if (count($date_array) === 3) {
		$month = $date_array[0];
		$day = $date_array[1];
		$year = $date_array[2];
	} else {
		echo 'Wrong date format: mm/dd/yyyy';
		exit();
	}

	return mktime($hour, $minute, $second, $month, $day, $year) * 1000;
}
/**
 * Generates a random number of a given length.
 */
function generate_id($length) {
	for ($i = 0, $id = ''; $i < $length; $i++)
		$id .= mt_rand(0, 9);
	return $id;
}
/**
 * Generates a GUID.
 */
function generate_uuid() {
	$low = generate_id(8);
	$mid = generate_id(4);
	$high = generate_id(4);
	$seq = generate_id(4);
	$node = generate_id(12);
	return $low . '-' . $mid . '-' . $high . '-' . $seq . '-' . $node;
}
