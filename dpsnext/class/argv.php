<?php

for ($i = 1; $i < count($argv); $i++) {
	$split = explode(':', $argv[$i]);
	$param = $split[0];
	$value = $split[1];

	switch ($param) {
		// credentials
		case 'client_id':
			$client_id = $value; break;
		case 'client_request_id':
			$client_request_id = $value; break;
		case 'client_session_id':
			$client_session_id = $value; break;
		case 'client_version':
			$client_version = $value; break;
		case 'client_upload_id':
			$client_upload_id = $value; break;
		case 'publication_id':
			$publication_id = $value; break;
		// endpoints
		case 'jupiter_endpoint':
			$jupiter_endpoint = $value; break;
		case 'jupiter_ingestion':
			$jupiter_ingestion = $value; break;
		// global parameters
		case 'article_version_id':
			$article_version_id = $value; break;
		case 'article_contentUrl':
			$article_contentUrl = $value; break;
		case 'entity_name':
			$entity_name = $value; break;
		case 'article_file':
			$article_file = $value; break;
		case 'show_debug':
			$show_debug = $value; break;
		case 'show_input':
			$show_input = $value; break;
		// article parameters
		case 'article_title':
			$article_title = $value; break;
		case 'article_accessState':
			$article_accessState = $value; break;
		case 'article_relatedContent':
			$article_relatedContent = $value; break;
		case 'article_byline':
			$article_byline = $value; break;
		case 'article_keywords':
			$article_keywords = $value; break;
		case 'article_importance':
			$article_importance = $value; break;
		case 'article_isAd':
			$article_isAd = $value; break;
		case 'article_description':
			$article_description = $value; break;
		case 'article_hideFromBrowsePage':
			$article_hideFromBrowsePage = $value; break;
		case 'article_bylineUrl':
			$article_bylineUrl = $value; break;
		default:
			break;
	}
}