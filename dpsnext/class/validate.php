<?php
/* ************************************************************************
 * NOTICE: This work is licensed under a Creative Commons Attribution
 * -Noncommercial-Share Alike 3.0 Unported License. Permissions beyond
 * the scope of this license, pertaining to the examples of code included
 * within this work are available at Adobe Commercial Rights.
 *
 * NOTE: You may reuse these files as you want, but they are not officially
 * supported as part of the product. Technical support will not be able to
 * answer questions about these files.
 *
 * Related Links:
 *  - http://creativecommons.org/licenses/by-nc-sa/3.0/
 *  - http://www.adobe.com/communities/guidelines/ccplus/commercialcode_plus_permission.html
 **************************************************************************/

/**
 * Custom class that handles the validation of parameters.
 */
class validate {
	private $missing;
	private $valid;

	public function __construct() {
		$this->missing = array();
		$this->valid = true;
	}

	public function param($type, $name, $value, $toggle = false) {
		if (!isset($value) || $value === '') {
			$this->valid = false;
			switch ($type) {
				case 'create':
					$this->_index_create($name); break;
				case 'credential':
					$this->_index_credential($name); break;
				case 'parameter':
					$this->_index_parameter($name); break;
				default:
					break;
			}
		} else if ($toggle) {
			$this->_check_file($name, $value);
		}
	}

	public function notValid() {
		if (!$this->valid) {
			$this->_output();
		}
		return !$this->valid;
	}

	private function _check_file($name, $value) {
		if (!file_exists(realpath($value))) {
			$this->valid = false;
			$info['desc'] = 'Invalid file path @ ' . $value;
			$info['name'] = $name;
			$info['type'] = 'Wrong File Path';
			$this->missing[] =  $info;
		}
	}

	private function _index_credential($value) {
		$info['desc'] = 'Update value in config/credential.php, on line #';
		$info['name'] = $value;
		$info['type'] = 'Missing Parameter';
		switch ($value) {
			case 'client_id':
				$info['desc'] .= '25'; break;
			case 'client_request_id':
				$info['desc'] .= '35'; break;
			case 'client_session_id':
				$info['desc'] .= '46'; break;
			case 'client_version':
				$info['desc'] .= '56'; break;
			case 'client_upload_id':
				$info['desc'] .= '64'; break;
			case 'publication_id':
				$info['desc'] .= '75'; break;
			default:
				break;
		}
		$this->missing[] =  $info;
	}

	private function _index_parameter($value) {
		$info['desc'] = 'Update value in config/parameter.php on line #';
		$info['name'] = $value;
		$info['type'] = 'Missing Parameter';
		switch ($value) {
			case 'article_version_id':
				$info['desc'] .= '29'; break;
			case 'article_contentUrl':
				$info['desc'] .= '39'; break;
			case 'entity_name':
				$info['desc'] .= '56'; break;
			case 'article_file':
				$info['desc'] .= '66'; break;
			case 'show_debug':
				$info['desc'] .= '75'; break;
			case 'show_input':
				$info['desc'] .= '84'; break;
			default:
				break;
		}
		$this->missing[] =  $info;
	}

	private function _output() {
		echo 'Please fill in the following values in their respective files:';
		echo '<br/><br/>';
		echo '<table>';
		echo '<thead>';
		echo '<tr>';
		echo 	'<th>Error</th>';
		echo 	'<th>Value</th>';
		echo 	'<th>Description</th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($this->missing as $value) {
			echo '<tr>';
			echo 	'<td>' . $value['type'] . '</td>';
			echo 	'<td style="padding-left:20px;">$';
			echo 		$value['name'];
			echo 	'</td>';
			echo 	'<td style="padding-left:20px;">';
			echo 		$value['desc'];
			echo 	'</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '</table>';
	}
}
